<?php // $Id$ ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="<?php echo $language->language ?>" lang="<?php echo $language->language ?>" <?php if (isset($language->dir)) { echo 'dir="'.$language->dir.'"'; } ?>>

  <head>
    <title><?php echo $head_title; ?></title>
    <?php echo $head; ?>
    <?php echo $styles; ?>
    <?php echo $scripts; ?>
    <!--[if IE 6]><link rel="stylesheet" href="<?php echo $base_path . $directory; ?>/style.ie6.css" type="text/css" /><![endif]-->
    <!--[if IE 7]><link rel="stylesheet" href="<?php echo $base_path . $directory; ?>/style.ie7.css" type="text/css" media="screen" /><![endif]-->
    <script type="text/javascript" src="<?php echo base_path() . path_to_theme() ?>/js/slidysearch.js"></script>
  </head>

  <body<?php echo sea_breeze_body_class($left, $right, $top_right); ?>>
    <div class="wrapper">
    <div id="page">
    <div id="metatools-bg_box"></div>
    <div id="header">
      <div id="metatoolbox">
        <div id="metatool-search">
          <?php echo $search_box; ?>
        </div>
      </div>
      <div id="headercontent">
        <?php if ($site_name): ?>
          <h1><a href="<?php echo $base_path; ?>" title="<?php echo t('Home'); ?>"><?php echo($site_name); ?></a></h1>
        <?php endif; ?>
        <?php if ($site_slogan): ?>
          <h2><?php echo($site_slogan); ?></h2>
        <?php endif; ?>
      </div>
    </div>
    <div id="menu">
      <?php if (!empty($navigation)): ?>
        <div class="nav">
          <div class="l"></div>
          <div class="r"></div>
          <?php echo $navigation; ?>
        </div>
      <?php endif; ?>
    </div>
    <div style="clear: both;"></div>
    <div id="contentwrapper">
      <div class="container">
        <div class="left">
          <div class="top">
            <div class="right">
              <div class="bottom">
                <div class="lb">
                  <div class="rt">
                    <div class="rb">
                      <div class="lt">
                        <div class="content-padding">
                          <?php if ($breadcrumb): echo $breadcrumb; endif ?>
                          <?php if ($left): ?>
                            <div id="sidebar-left" class="sidebar">
                              <div class="region-content"><?php echo $left; ?></div>
                            </div>
                          <?php endif; ?>
                          <div id="main">
                            <div id="featured">
                              <div class="col_bg">
                                <div class="left_2">
                                  <div class="top_2">
                                    <div class="right_2">
                                      <div class="bottom_2">
                                        <div class="lb_2">
                                          <div class="rt_2">
                                            <div class="rb_2">
                                              <div class="lt_2">
                                                <?php if ($featured): ?>
                                                  <div class="region-content">
                                                    <?php echo $featured; ?>
                                                  </div>
                                                <?php endif; ?>
                                                <?php if ($mission): ?>
                                                  <div class="region-content">
                                                    <div id="mission">
                                                      <?php echo $mission; ?>
                                                    </div>
                                                  </div>
                                                <?php endif; ?>
                                              </div>
                                            </div>
                                          </div>
                                        </div>
                                      </div>
                                    </div>
                                  </div>
                                </div>
                              </div>
                            </div>
                            <div style="clear: both;"></div>
                            <?php if ($tabs): ?>
                              <div class="tabs">
                                <?php echo $tabs; ?>
                              </div>
                            <?php endif; ?>
                            <?php if ($title): ?>
                              <h1 class="title"><?php echo $title; ?></h1>
                            <?php endif; ?>
                            <?php echo $help; ?>
                            <?php echo $messages; ?>
                            <?php echo $content; ?>
                            <?php echo $feed_icons; ?>
                          </div>
                          <?php if ($right || $top_right): ?>
                            <div id="sidebar-right" class="sidebar">
                              <?php if ($top_right): ?>
                                <div id="top_right">
                                  <div class="col_bg">
                                    <div class="left_2">
                                      <div class="top_2">
                                        <div class="right_2">
                                          <div class="bottom_2">
                                            <div class="lb_2">
                                              <div class="rt_2">
                                                <div class="rb_2">
                                                  <div class="lt_2">
                                                    <div class="region-content">
                                                      <?php echo $top_right; ?>
                                                    </div>
                                                  </div>
                                                </div>
                                              </div>
                                            </div>
                                          </div>
                                        </div>
                                      </div>
                                    </div>
                                  </div>
                                </div>
                              <?php endif; ?>
                              <?php if ($right): ?>
                                <?php echo $right; ?>
                              <?php endif; ?>
                            </div>
                            <?php endif; ?>
                            <div style="clear: both;"></div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="footer">
        <div class="footer_left">
          <?php echo $footer_left; ?>
        </div>
        <div class="footer_middle">
          <?php echo $footer_middle; ?>
        </div>
        <div class="footer_right">
          <?php echo $footer_right; ?>
        </div>
        <div class="footerCopy">
          <div class="left">
            <?php echo $footer_message; ?>
          </div>
          <div class="right">Developed by <a href="http://agileware.net">Agileware Pty Ltd</a></div>
        </div>
      </div>
      <?php echo $closure; ?>
    </div>
    </div>
  </body>
</html>