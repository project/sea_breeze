
 if (Drupal.jsEnabled) {
              // Drupal's JS "kill switch".
              // Don't do anything if browser isn't compatible.
    $(document).ready(function(){
              // JQuery's trick for running scripts before
              // the page has finished loading
      sheight = $("#metatoolbox").height();
              // get the height of the #search div
      soffset = -1 * (sheight - 14);
              // offset is search height minus 14px for the tab
      $('#metatoolbox').css({top: soffset});
              // move the #search div up so that only the tab is exposed
      $('#metatoolbox').hover(
              // defines what happens when you hover over the #search div
        function() {$(this).animate({top:0}, 'slow', "bounceout", function(){$('#edit-search-theme-form-1').focus();})},
             // on mouseover: animate the position to 0
             // at slow speed using bounceout easing,
             // cursor in textfield when done
        function() {setTimeout("$('#metatoolbox').animate({top:soffset}, 'normal', 'bounceout')", 300)}
             // on mouseout, wait 300ms,
             // then animate back up at normal speed
             // using bounceout easing
      );
    });
  }

  // this next bit defines the easing
  // this function was nicked from the Interface library:
  // http://interface.eyecon.ro
  // I've chosen to just include this with the script
  // rather than including the entire
  // interface.js file on every page
  jQuery.easing.bounceout = function(p, n, firstNum, delta, duration) {
    if ((n/=duration) < (1/2.75)) {
      return delta*(7.5625*n*n) + firstNum;
    } else if (n < (2/2.75)) {
      return delta*(7.5625*(n-=(1.5/2.75))*n + .75) + firstNum;
    } else if (n < (2.5/2.75)) {
      return delta*(7.5625*(n-=(2.25/2.75))*n + .9375) + firstNum;
    } else {
      return delta*(7.5625*(n-=(2.625/2.75))*n + .984375) + firstNum;
    }
  };